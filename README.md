# Overview

Dies ist eine Übersicht über einige meiner Projekte.

## [CS229: Machine Learning](https://github.com/Joker14641/cs229)
- Die verlinkte Repository enthält von mir ausgearbeitete Lösungen zu den Übungsaufgaben der Vorlesung "CS229: Machine Learning" der Stanford University aus dem Herbstquartal 2018. Die zugehörigen Kursmaterialien wurden im April 2020 öffentlich zugänglich gemacht.

**Änderung 23.11.2020:** Auf Wunsch des CS229-Teams habe ich die Sichtbarkeit meiner Repository auf privat geändert. Bei Interesse an dem Code kontaktieren Sie mich daher bitte über die E-Mail in meinem Lebenslauf. Die Kursinhalte kann man [hier](http://cs229.stanford.edu/syllabus-autumn2018.html) nachlesen.
- Im Vergleich mit anderen mir bekannten Kursen zu Machine Learning wird in CS229 wesentlich ausführlicher auf die zugrundeliegende Mathematik eingegangen. Unter anderem werden folgende Themen behandelt:
  - *Supervised Learning*: Verallgemeinerte lineare Modelle (z.B. lineare bzw. logistische Regression), Gaussian Discriminant Analysis (GDA), Naiver Bayes-Klassifikator, Support Vector Machines (SVM), Decision Trees, Neuronale Netze
  - *Unsupervised Learning*: K-Means-Clustering, Expectation-Maximization (EM), Factor Analysis
  - *Reinforcement Learning*: Markow-Entscheidungsprobleme, Linear Quadratic Regulation, Differential Dynamic Programming, Linear Quadratic Gaussian
- Begleitend zu den Vorlesungen gibt es vier Übungszettel mit sowohl Theorie als auch Programmieraufgaben. Ich habe den vorgegebenen Startercode in IPython-Notebooks umstrukturiert und meine Lösungen so ausgearbeitet, so dass diese bequem in Github gelesen werden können. Außerdem habe ich stellenweise mathematische Abstraktionen ergänzt, die nicht in der Vorlesung behandelt wurden, aber meiner Meinung nach einige Herleitungen vereinfachen. 
- Nachfolgend eine Auswahl von Übungsaufgaben, die ich besonders interessant finde:
  - **Binäre Klassifikation mit incomplete positive-only Labels**: Die Problemstellung besteht darin ein binäres Klassifikationsmodell zu entwickeln, bei dem während des Trainings nicht auf alle Label zugegriffen werden kann, sondern nur auf einen Teil der positiven Labels. Durch einen wahrscheinlichkeitstheoretischen Trick lässt sich bei einem solchen Modell trotzdem eine akzeptable Präzision erreichen.
    - *Anwendungsbeispiel*: Gegeben ist eine Datenbank von Proteinen und es soll untersucht werden, welche Proteine an einem bestimmten chemischen Prozess beteiligt sind. Von einigen Proteinen ist bekannt, dass sie an dem Prozess beteiligt sind, aber bei vielen ist dies unklar.
    - Die folgende Grafik zeigt einen Datensatz mit zwei Klassen (blaue Kreuze bzw. gelbe Sterne), die linear getrennt werden sollen. Das Modell in der linken Spalte hat während des Trainings nur Zugriff auf einen Teil der positiven Labels (blaue Kreuze). Zum Vergleich zeigt die rechte Spalte ein GDA-Modell, das Zugriff auf alle Labels hat: ![GDA_incomplete_pos_only](/img/GDA_incomplete_pos_only.png)
  - **Spam-Klassifizierung** mit Naive Bayes bzw. mit einer SVM
  - **Semi-Supervised EM** Mit dem klassische EM-Algorithmus kann man Daten ohne Label verschiedene Klassen zuordnen. Der Algorithmus lässt sich aber auch für den Fall adaptieren, in dem zumindest für einen Teil der Daten die echten Labels bekannt sind. Die erste Zeile der folgende Grafik zeigt einen solchen Datensatz. Mit diesem wurden ein klassisches und ein adaptiertes Gaussian-Mixture-Model (GMM) bis zur Konvergenz trainiert. Die Tabelle zeigt die entstandenen Klassifikationen zu jeweils drei unterschiedlichen randomisierten Startparametern: ![Semi-supervised EM](/img/Semi-supervised EM.png)
  - **Komprimierung mit K-Means**: Die nächste Grafik zeigt ein Bild, dass im Original in 24-bit vorlag (entspricht fast 17 Millionen möglichen Farben) und mit dem K-Means-Algorithmus komprimiert wurde: ![K-Means](/img/K-means for compression.png)
  - **MNIST-Klassifikation** (handgeschriebene Ziffern) mit einem mit Numpy implementierten Convolutional Neural Network inklusive Herleitung und Implementierung der Zwischenschritte für die Backpropagation 
  - **Cocktail-Party-Problem**: Aus mehreren Tonaufnahmen überlagerter (unverständlicher) Gespräche kann man mit Independent Component Analysis herausfinden, was jeder einzelne Sprecher gesagt hat.

## [Gamedil](https://gitlab.com/joker14641/gamedil)
- In diesem Projekt habe ich ein Browserspiel für zwei Spieler entwickelt, das man [hier](http://gamedil.herokuapp.com/) online spielen kann. ![gamedil](/img/gamedil.png)
- Vergleichbar mit dem Spiel Worms, kann sich in Gamedil die Person, die gerade am Zug ist, auf dem Spielfeld bewegen und eines von zwei Geschossen benutzen, das beim Aufprall das Terrain teilweise zerstört. Eines der Geschosse explodiert beim ersten Kontakt mit dem Terrain, das andere kann auch auf dem Boden rollen und von diesem abprallen. Mein Hauptinteresse bei diesem Projekt war die Entwicklung der Algorithmen, die für die Kollisionserkennung und die Physiksimulation notwendig sind.
- Das Backend des Spiels ist in Node.js implementiert. Neben dem Hauptspiel gibt es auch noch eine Chatfunktion und die Möglichkeit Schere-Stein-Papier zu spielen.
